package com.ads.arcanoid.View;

import com.ads.arcanoid.ArcanoidGame;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by Гриша on 27.01.2016.
 */
public class ImageActor extends Actor {
    TextureRegion img;
    public ImageActor(TextureRegion img, float x, float y, float width, float height){
        this.img = img;
        setPosition(x, y);
        setSize(width, height);
    }
    public ImageActor(Texture img, float x, float y){
        this(new TextureRegion(img), x, y, img.getWidth(), img.getHeight());
    }
    public ImageActor(Texture img, float x, float y, float width, float height){
        this(new TextureRegion(img), x, y, width, height);
    }
    public ImageActor(TextureRegion img, float x, float y){
        this(img, x, y, img.getRegionWidth(), img.getRegionHeight());
    }

    @Override
    public void draw(Batch batch, float parentAlpha){
        batch.draw(img,
                getX(),
                getY(),
                getWidth(),
                getHeight());
    }
}
