package com.ads.arcanoid.Model;

import com.ads.arcanoid.View.Ball;
import com.ads.arcanoid.View.ImageActor;
import com.ads.arcanoid.View.Player;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.HashMap;

/**
 * Created by ga_nesterchuk on 10.02.2016.
 */
public class World extends Stage {
    private ImageActor background;
    /*
    private ImageActor[] brick;
    private ImageActor[][] bricks;*/
    private Player player;
    private Ball ball;
    private float SPEED_MULT = 2.5f;


    class PlayerMover extends ClickListener {
        public void clicked(InputEvent event, float x, float y) {
            if (2 * x > Gdx.graphics.getWidth())
                player.setX(player.getX() + 3);
            else
                player.setX(player.getX() - 3);
        }
    }

    public World(ScreenViewport screenViewport, SpriteBatch batch, HashMap<String, TextureRegion> textureRegions) {
        super(screenViewport, batch);
        /*brick = new ImageActor[10];
        brick[0] = new ImageActor(textureRegions.get("blue-brick"), 0, 0);
        brick[1] = new ImageActor(textureRegions.get("blue-small-stick"), 0, 32);
        brick[2] = new ImageActor(textureRegions.get("blue-normal-stick"), 0, 64);
        brick[3] = new ImageActor(textureRegions.get("blue-long-stick"), 0, 96);
        brick[4] = new ImageActor(textureRegions.get("blue-longest-stick"), 0, 128);
        brick[5] = new ImageActor(textureRegions.get("blue-small-ball"), 0, 160);
            */
        /*
        bricks = new ImageActor[11][25];
        for(int i = 0; i < 11; i++){
            for (int j = 0; j<25 - i%2; j++) {
                bricks[i][j] = new ImageActor(textureRegions.get("blue-brick"), 60 + i % 2 * 16 + j * 32, 260 + 16 * i);
                addActor(bricks[i][j]);
            }
        }
        for(int i = 0; i<6; i++)
            addActor(brick[i]);
            */
        background = new ImageActor(textureRegions.get("blank background"), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        addActor(background);
        player = new Player(textureRegions.get("blue-normal-stick"), Gdx.graphics.getWidth() / 2, 0, 80 * SPEED_MULT);
        addActor(player);
        ball = new Ball(textureRegions.get("blue-big-ball"), Gdx.graphics.getWidth() / 2, player.getHeight(), 60 * SPEED_MULT, 60);
        addActor(ball);
        reset();
    }

    public void reset() {
        ball.setPosition(player.getX() + player.getWidth() / 2, player.getHeight());
    }


    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.A:
                player.setAction(Player.Action.MOVELEFT);
                break;
            case Input.Keys.D:
                player.setAction(Player.Action.MOVERIGHT);
                break;
        }
        return false;
    }

    public boolean keyUp(int keycode) {
        switch (keycode) {
            case Input.Keys.A:
            case Input.Keys.D:
                player.setAction(Player.Action.STAND);
        }
        return false;
    }

    public Player getPlayer() {
        return player;
    }

/*
    public boolean keyTyped (char character) {
        return false;
    }

    public boolean touchDown (int screenX, int screenY, int pointer, int button) {
        return false;
    }

    public boolean touchUp (int screenX, int screenY, int pointer, int button) {
        return false;
    }

    public boolean touchDragged (int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved (int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled (int amount) {
        return false;
    }
*/
}
